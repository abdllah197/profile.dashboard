import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: '' },
    { path: '/project', title: 'Project',  icon: 'library_books', class: '' },
    { path: '/project-type', title: 'Project Type',  icon: 'library_books', class: '' },
    { path: '/skills', title: 'Skills',  icon: 'library_books', class: '' },
    { path: '/certificates', title: 'Certificates',  icon: 'library_books', class: '' },
    { path: '/education', title: 'Education',  icon: 'library_books', class: '' },
    { path: '/experiences', title: 'Experience',  icon: 'library_books', class: '' },
    { path: '/home', title: 'Home',  icon: 'library_books', class: '' },
    { path: '/services', title: 'Services',  icon: 'library_books', class: '' },
    { path: '/services-info', title: 'Services Info',  icon: 'library_books', class: '' },
    { path: '/contactUs', title: 'Contact Us',  icon: 'library_books', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
