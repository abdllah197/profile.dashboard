import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AdminLayoutRoutes } from "./admin-layout.routing";
import { DashboardComponent } from "../../dashboard/dashboard.component";
import { UserProfileComponent } from "../../user-profile/user-profile.component";
import { TableListComponent } from "../../table-list/table-list.component";
import { TypographyComponent } from "../../typography/typography.component";
import { IconsComponent } from "../../icons/icons.component";
import { MapsComponent } from "../../maps/maps.component";
import { NotificationsComponent } from "../../notifications/notifications.component";
import { UpgradeComponent } from "../../upgrade/upgrade.component";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatRippleModule } from "@angular/material/core";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatSelectModule } from "@angular/material/select";
import { BranchComponent } from "app/branch/branch.component";
import { DataTablesModule } from "angular-datatables";
import { ProjectComponent } from "../../project/project.component";
import {CertificatesComponent} from "../../certificates/certificates.component"
import {EducationComponent} from "../../education/education.component"
import {ExperiencesComponent} from "../../experiences/experiences.component"
import {HomeComponent} from "../../home/home.component"
import {ProjectTypeComponent} from "../../projectType/projectType.component"
import {ServicesComponent} from "../../services/services.component"
import {ServicesInfoComponent} from "../../servicesInfo/servicesInfo.component"
import {SkillsComponent} from "../../skills/skills.component"
import {ContactUsComponent} from "../../contactUs/contactUs.component"

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    DataTablesModule,
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    BranchComponent,
    ProjectComponent,
    CertificatesComponent,
    EducationComponent,
    ExperiencesComponent,
    HomeComponent,
    ProjectTypeComponent,
    ServicesComponent,
    ServicesInfoComponent,
    SkillsComponent,
    ContactUsComponent,
  ],
})
export class AdminLayoutModule {}
