import { Routes } from "@angular/router";
import { DashboardComponent } from "../../dashboard/dashboard.component";
import { UserProfileComponent } from "../../user-profile/user-profile.component";
import { TableListComponent } from "../../table-list/table-list.component";
import { TypographyComponent } from "../../typography/typography.component";
import { IconsComponent } from "../../icons/icons.component";
import { MapsComponent } from "../../maps/maps.component";
import { NotificationsComponent } from "../../notifications/notifications.component";
import { UpgradeComponent } from "../../upgrade/upgrade.component";
import { BranchComponent } from "app/branch/branch.component";
import { ProjectComponent } from "app/project/project.component";
import {CertificatesComponent} from "app/certificates/certificates.component"
import {EducationComponent} from "app/education/education.component"
import {ExperiencesComponent} from "app/experiences/experiences.component"
import {HomeComponent} from "app/home/home.component"
import {ProjectTypeComponent} from "app/projectType/projectType.component"
import {ServicesComponent} from "app/services/services.component"
import {ServicesInfoComponent} from "app/servicesInfo/servicesInfo.component"
import {SkillsComponent} from "app/skills/skills.component"
import {GuardService} from "../../core/Auth/guard.service"
import {BranchIdService} from "../../core/Branch/branch-id.service"
import { ContactUsComponent } from "app/contactUs/contactUs.component";

export const AdminLayoutRoutes: Routes = [
  // {
  //   path: '',
  //   children: [ {
  //     path: 'dashboard',
  //     component: DashboardComponent
  // }]}, {
  // path: '',
  // children: [ {
  //   path: 'userprofile',
  //   component: UserProfileComponent
  // }]
  // }, {
  //   path: '',
  //   children: [ {
  //     path: 'icons',
  //     component: IconsComponent
  //     }]
  // }, {
  //     path: '',
  //     children: [ {
  //         path: 'notifications',
  //         component: NotificationsComponent
  //     }]
  // }, {
  //     path: '',
  //     children: [ {
  //         path: 'maps',
  //         component: MapsComponent
  //     }]
  // }, {
  //     path: '',
  //     children: [ {
  //         path: 'typography',
  //         component: TypographyComponent
  //     }]
  // }, {
  //     path: '',
  //     children: [ {
  //         path: 'upgrade',
  //         component: UpgradeComponent
  //     }]
  // }
  { path: "dashboard", component: DashboardComponent, canActivate :[GuardService,BranchIdService]},
  { path: "branch", component: BranchComponent, canActivate :[GuardService,BranchIdService] },
  { path: "project", component: ProjectComponent, canActivate :[GuardService,BranchIdService] },
  { path: "user-profile", component: UserProfileComponent, canActivate :[GuardService,BranchIdService] },
  { path: "table-list", component: TableListComponent, canActivate :[GuardService,BranchIdService] },
  { path: "typography", component: TypographyComponent, canActivate :[GuardService,BranchIdService] },
  { path: "icons", component: IconsComponent, canActivate :[GuardService,BranchIdService] },
  { path: "maps", component: MapsComponent, canActivate :[GuardService,BranchIdService] },
  { path: "notifications", component: NotificationsComponent, canActivate :[GuardService,BranchIdService] },
  { path: "upgrade", component: UpgradeComponent, canActivate :[GuardService,BranchIdService] },
  { path: "certificates", component: CertificatesComponent, canActivate :[GuardService,BranchIdService] },
  { path: "education", component: EducationComponent, canActivate :[GuardService,BranchIdService] },
  { path: "experiences", component: ExperiencesComponent, canActivate :[GuardService,BranchIdService] },
  { path: "home", component: HomeComponent, canActivate :[GuardService,BranchIdService] },
  { path: "project-type", component: ProjectTypeComponent, canActivate :[GuardService,BranchIdService] },
  { path: "services", component: ServicesComponent, canActivate :[GuardService,BranchIdService] },
  { path: "services-info", component: ServicesInfoComponent, canActivate :[GuardService,BranchIdService] },
  { path: "skills", component: SkillsComponent, canActivate :[GuardService,BranchIdService] },
  { path: "contactUs", component: ContactUsComponent, canActivate :[GuardService,BranchIdService] },
  { path: '404', redirectTo:'dashboard'},
  { path: '**', redirectTo:'dashboard'}
];
