import { BranchDto } from '../../branch/Models/BranchDto';

export class SkillsDto {
  id: number = 0;
  titleAr: string = "";
  titleEn: string = "";
  date: Date = new Date();
  isActive:boolean=false;
  isEducation:boolean=false;
  isExperience:boolean=false;
  rate: number=0;
  branchId: Array<BranchDto> = new Array<BranchDto>();
}
export class GenericSkillsDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllSkills";
  UrlAdd = "CreateSkill";
  UrlEdit = "UpdateSkill";
  UrlDelete = "DeleteSkill";
  UrlSelect = "GetAllBranches";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "branchDtoId") {
      return "option";
    }
    if (key == "date") {
      return "date";
    }
    if (key == "dateTo") {
      return "date";
    }
    if (key == "dateFrom") {
      return "date";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "date": {
        return false;
      }
      case "titleAr": {
        return true;
      }
      case "titleEn": {
        return true;
      }
      case "rate": {
        return true;
      }
      
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "isEducation": {
        return "Education";
      }
      case "isExperience": {
        return "Experience";
      }
      case "date": {
        return "Date";
      }
      case "rate": {
        return "Rate";
      }
      case "dateTo": {
        return "Date To";
      }
      case "dateFrom": {
        return "Date From";
      }
      case "titleAr": {
        return "title Ar";
      }
      case "titleEn": {
        return "title En";
      }
      case "placeAr": {
        return "place Ar";
      }
      case "placeEn": {
        return "place En";
      }
      case "SubTitleAr": {
        return "Subtitle Ar";
      }
      case "SubTitleEn": {
        return "Subtitle En";
      }
      case "descriptionAr": {
        return "description Ar";
      }
      case "descriptionEn": {
        return "description En";
      }
      case "image": {
        return "image";
      }
      case "branchId": {
        return "branchId";
      }
      case "branchNameAr": {
        return "branch Name Ar";
      }
      case "branchNameEn": {
        return "branch Name En";
      }
      case "CvLink": {
        return "CV Link";
      }
      case "Facbook": {
        return "Facbook";
      }
      case "Twitter": {
        return "Twitter";
      }
      case "LinkedIn": {
        return "LinkedIn";
      }
      case "VideoLink": {
        return "Video Link";
      }
      case "BackgroundImage": {
        return "Background Image";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }
    }
  };
}
