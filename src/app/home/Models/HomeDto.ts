import { BranchDto } from '../../branch/Models/BranchDto';

export class HomeDto {
  id: number = 0;
  titleAr: string = "";
  titleEn: string = "";
  descriptionAr: string = "";
  descriptionEn: string = "";
  subTitleAr: string = "";
  subTitleEn: string = "";
  date: Date = new Date();
  isActive:boolean=false;
  cvLink: string = "";
  vedioLink: string = "";
  linkedIn: string = "";
  facebook: string = "";
  twitter: string = "";
  backgroundImage: File = new File([""], "", {
    type: "",
  });
  branchId: Array<BranchDto> = new Array<BranchDto>();
}
export class GenericHomeDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllHomes";
  UrlAdd = "CreateHome";
  UrlEdit = "UpdateHome";
  UrlDelete = "DeleteHome";
  UrlSelect = "GetAllBranches";
  switchClass = (key) => {
    if (key == "backgroundImage") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "branchDtoId") {
      return "option";
    }
    if (key == "date") {
      return "date";
    }
    if (key == "dateTo") {
      return "date";
    }
    if (key == "dateFrom") {
      return "date";
    }
    if (key == "backgroundImage" || key == "video" || key == "image") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "date": {
        return false;
      }
      case "titleAr": {
        return true;
      }
      case "titleEn": {
        return true;
      }
      case "descriptionAr": {
        return true;
      }
      case "descriptionEn": {
        return true;
      }
      case "subTitleAr": {
        return true;
      }
      case "subTitleEn": {
        return true;
      }
      case "cvLink": {
        return true;
      }
      case "vedioLink": {
        return true;
      }
      case "linkedIn": {
        return true;
      }
      case "facebook": {
        return true;
      }
      case "twitter": {
        return true;
      }
      case "backgroundImage": {
        return true;
      }
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "backgroundImage": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "date": {
        return "Date";
      }
      case "dateTo": {
        return "Date To";
      }
      case "dateFrom": {
        return "Date From";
      }
      case "titleAr": {
        return "title Ar";
      }
      case "titleEn": {
        return "title En";
      }
      case "placeAr": {
        return "place Ar";
      }
      case "placeEn": {
        return "place En";
      }
      case "subTitleAr": {
        return "Subtitle Ar";
      }
      case "subTitleEn": {
        return "Subtitle En";
      }
      case "descriptionAr": {
        return "description Ar";
      }
      case "descriptionEn": {
        return "description En";
      }
      case "backgroundImage": {
        return "background Image";
      }
      case "branchId": {
        return "branchId";
      }
      case "branchNameAr": {
        return "branch Name Ar";
      }
      case "branchNameEn": {
        return "branch name En";
      }
      case "cvLink": {
        return "CV Link";
      }
      case "facebook": {
        return "Facebook";
      }
      case "twitter": {
        return "Twitter";
      }
      case "linkedIn": {
        return "LinkedIn";
      }
      case "vedioLink": {
        return "Video Link";
      }
      
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }
    }
  };
}
