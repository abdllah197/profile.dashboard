import { ProjectTypeDto } from '../../projectType/Models/ProjectTypeDto';

export class ProjectDto {
  id: number = 0;
  titleAr: string = "";
  titleEn: string = "";
  isActive: boolean = false;
  date: Date = new Date();
  image: File = new File([""], "", {
    type: "",
  });
  linkWebsite: string = "";
  linkIos: string = "";
  linkAndroid: string = "";
  projectTypesId: Array<ProjectTypeDto> = new Array<ProjectTypeDto>();
  projectTypeTitleAr: string = "";
  projectTypeTitleEn: string = "";
  projectTypeBranchId: number = 0;
}
export class GenericProjectDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyIgnore = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllProjects";
  UrlAdd = "CreateProject";
  UrlEdit = "UpdateProject";
  UrlDelete = "DeleteProject";
  UrlSelect = "GetAllProjectTypes";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  CheckIgnore = (key) => {
      if (key == "projectTypeTitleAr") {
        return "ignore";
      }
      if (key == "projectTypeTitleEn") {
        return "ignore";
      }
  }
  
  switchValue = (value, key) => {
   
    if (key == "date") {
      return "date";
    }
    if (key == "dateTo") {
      return "date";
    }
    if (key == "dateFrom") {
      return "date";
    }
    if (key == "projectTypesId") {
      return "option";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }    
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "date": {
        return false;
      }
      case "titleAr": {
        return true;
      }
      case "titleEn": {
        return true;
      }
      case "image": {
        return true;
      } 
      case "linkAndroid": {
        return true;
      }
      case "linkIos": {
        return true;
      }
      case "linkWebsite": {
        return true;
      } 
      case "projectTypesId": {
        return true;
      }    
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "date": {
        return "Date";
      }
      case "titleAr": {
        return "title Ar";
      }
      case "titleEn": {
        return "title En";
      }
      case "image": {
        return "image";
      }
      case "linkWebsite": {
        return "link Website";
      }
      case "linkIos": {
        return "link Ios";
      }
      case "linkAndroid": {
        return "link Android";
      }
      case "projectTypesId": {
        return "projectTypes";
      }
      case "projectTypeTitleAr": {
        return "project Type Title Ar";
      }
      case "projectTypeTitleEn": {
        return "project Type Title En";
      }
      case "projectTypeBranchId": {
        return "branchId";
      }
      case "branchId": {
        return "branchId";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }
      case "projectTypesId": {
        return true;
      }
      case "projectTypeTitleAr": {
        return false;
      }
      case "projectTypeTitleEn": {
        return false;
      }
      case "projectTypeBranchId": {
        return true;
      }
    }
  };
}
