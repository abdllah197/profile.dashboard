export class BranchDto {
  id: number = 0;
  nameAr: string = "";
  nameEn: string = "";
  image: File = new File([""], "", {
    type: "",
  });
  isActive: boolean = false;
  date: Date = new Date();
}
export class GenericBranchDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllBranches";
  UrlAdd = "CreateBranch";
  UrlEdit = "UpdateBranch";
  UrlDelete = "DeleteBranch";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "date") {
      return "date";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "date": {
        return false;
      }
      case "nameAr": {
        return true;
      }
      case "nameEn": {
        return true;
      }
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "date": {
        return "Date";
      }
      case "nameAr": {
        return "Name Ar";
      }
      case "nameEn": {
        return "Name En";
      }
      case "image": {
        return "image";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "image": {
        return false;
      }
    }
  };
}
