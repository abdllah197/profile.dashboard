import { BranchDto } from './../../branch/Models/BranchDto';

export class CertificatesDto {
  id: number = 0;
  titleAr: string = "";
  titleEn: string = "";
  descriptionAr: string = "";
  descriptionEn: string = "";
  isActive: boolean = false;
  date: Date = new Date();
  image: File = new File([""], "", {
    type: "",
  });
  branchId: Array<BranchDto> = new Array<BranchDto>();
}
export class GenericCertificatesDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllCertificates";
  UrlAdd = "CreateCertificates";
  UrlEdit = "UpdateCertificates";
  UrlDelete = "DeleteCertificates";
  UrlSelect = "GetAllBranches";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "branchDtoId") {
      return "option";
    }
    if (key == "date") {
      return "date";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "image": {
        return true;
      }
      case "titleAr": {
        return true;
      }
      case "titleEn": {
        return true;
      }
      case "descriptionAr": {
        return true;
      }
      case "descriptionEn": {
        return true;
      }
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "date": {
        return "Date";
      }
      case "titleAr": {
        return "title Ar";
      }
      case "titleEn": {
        return "title En";
      }
      case "descriptionAr": {
        return "description Ar";
      }
      case "descriptionEn": {
        return "description En";
      }
      case "image": {
        return "image";
      }
      case "branchId": {
        return "branchId";
      }
      case "branchNameAr": {
        return "branch Name Ar";
      }
      case "branchNameEn": {
        return "branch name En";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }     
    }
  };
}
