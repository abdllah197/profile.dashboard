import { BranchDto } from './../../branch/Models/BranchDto';

export class EducationDto {
  id: number = 0;
  titleAr: string = "";
  titleEn: string = "";
  descriptionAr: string = "";
  descriptionEn: string = "";
  dateTo: Date = new Date();
  dateFrom: Date = new Date();
  isActive:boolean=false;
  placeAr: string = "";
  placeEn: string = "";
  branchId: Array<BranchDto> = new Array<BranchDto>();
}
export class GenericEducationDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllEducations";
  UrlAdd = "CreateEducation";
  UrlEdit = "UpdateEducation";
  UrlDelete = "DeleteEducation";
  UrlSelect = "GetAllBranches";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "branchDtoId") {
      return "option";
    }
    if (key == "date") {
      return "date";
    }
    if (key == "dateTo") {
      return "date";
    }
    if (key == "dateFrom") {
      return "date";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "date": {
        return false;
      }
      case "titleAr": {
        return true;
      }
      case "titleEn": {
        return true;
      }
      case "descriptionAr": {
        return true;
      }
      case "descriptionEn": {
        return true;
      }
      case "dateTo": {
        return true;
      }
      case "dateFrom": {
        return true;
    }
  };
}
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "date": {
        return "Date";
      }
      case "dateTo": {
        return "Date To";
      }
      case "dateFrom": {
        return "Date From";
      }
      case "titleAr": {
        return "title Ar";
      }
      case "titleEn": {
        return "title En";
      }
      case "placeAr": {
        return "place Ar";
      }
      case "placeEn": {
        return "place En";
      }
      case "descriptionAr": {
        return "description Ar";
      }
      case "descriptionEn": {
        return "description En";
      }
      case "image": {
        return "image";
      }
      case "branchId": {
        return "branchId";
      }
      case "branchNameAr": {
        return "branch Name Ar";
      }
      case "branchNameEn": {
        return "branch name En";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }
    }
  };
}
