import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import Swal from "sweetalert2";
import { environment } from "../../../environments/environment";
import { Helper } from "../Helper";

@Injectable({
  providedIn: "root",
})
export class CoreCrudService {
  apiRoot: string = environment.baseUrl;
  //apiRoot: string ="https://localhost:44358/api/v1/";
  lang: string = "ar";
  headers: HttpHeaders | undefined;
  helper: Helper;

  constructor(private http: HttpClient, private router: Router) {
    const headers = { lang: this.lang };
    var token = JSON.parse(localStorage.getItem("token")!);
    if (!!token) {
      this.headers = new HttpHeaders(headers).set(
        "Authorization",
        `Bearer ${token}`
      );
    }

    this.helper = new Helper(this.router);
  }
  AddNew(AddDto: any, serviceName: string) {
    return this.http
      .post(this.apiRoot + serviceName, AddDto, { headers: this.headers })
      .subscribe(
        (res) => {
          Swal.fire({
            text: "تم الحفظ بنجاح",
            icon: "success",
          });
        },
        (err) => {
          //function handel status code
          if (err instanceof HttpErrorResponse) {
            console.log(err.error);
            this.helper.switchStatus(err.status, err.error.msg);
          }
        }
      );
  }
  GetData(AddDto: any, serviceName: string): Observable<any> {
    return this.http.get(this.apiRoot + serviceName, {
      params: AddDto,
      headers: this.headers,
    });
  }
  GetDataById(AddDto: any, serviceName: string) {
    return this.http.get(this.apiRoot + serviceName, {
      params: { id: AddDto },
      headers: this.headers,
    });
  }
  Update(AddDto: any, serviceName: string) {
    return this.http
      .put(this.apiRoot + serviceName, AddDto, {
        headers: this.headers,
      })
      .subscribe(
        (res) => {
          Swal.fire({
            text: "تم التعديل بنجاح",
            icon: "success",
          });
          // this.router.navigate(["/" + routeComponent + "/index"]);
        },
        (err) => {
          //function handel status code
          if (err instanceof HttpErrorResponse) {
            console.log(err.error);
            this.helper.switchStatus(err.status, err.error.msg);
          }
        }
      );
  }
  SendEmail(SendDto: any, serviceName: string) {
    return this.http
      .post(this.apiRoot + serviceName, SendDto, {
        headers: this.headers,
      })
      .subscribe(
        (res) => {
          Swal.fire({
            text: "تم الارسال بنجاح",
            icon: "success",
          });
          // this.router.navigate(["/" + routeComponent + "/index"]);
        },
        (err) => {
          //function handel status code
          if (err instanceof HttpErrorResponse) {
            console.log(err.error);
            this.helper.switchStatus(err.status, err.error.msg);
          }
        }
      );
  }
  ChangeStutes(AddDto: any, serviceName: string) {
    return this.http.patch(this.apiRoot + serviceName, AddDto, {
      headers: this.headers,
    });
  }
  Delete(AddDto: any, serviceName: string) {
    return this.http.delete(this.apiRoot + serviceName, {
      params: { id: AddDto },
      headers: this.headers,
    });
  }
}
