import { Router } from "@angular/router";
import Swal from "sweetalert2";

export class Helper {
  constructor(private router: Router) {}
  switchRole = (role: Array<string>) => {
    for (var i = 0; i < role?.length; i++) {
      switch (role[i]) {
        case "admin": {
          //console.log(role[i]);
          this.router.navigate(["/dashboard"]);
          break;
        }
        case "Advertisment": {
          this.router.navigate(["/alaeilanat/index"]);
          break;
        }
        case "Payments": {
          this.router.navigate(["/dashboard/index"]);
          break;
        }
        case "Copons": {
          this.router.navigate(["/Copons/index"]);
          break;
        }
        case "SocialMedia": {
          this.router.navigate(["/SocialMedias/index"]);
          break;
        }
        case "Questions": {
          this.router.navigate(["/QuestionClients/index"]);
          break;
        }
        case "Questions": {
          this.router.navigate(["/QuestionProviders/index"]);
          break;
        }
        case "Notifications": {
          this.router.navigate(["/dashboard/index"]);
          break;
        }
        case "SendSmsMsg": {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
        case "Chat": {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
        case "ContactUs": {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
        case "Setting": {
          this.router.navigate(["/setting/index"]);
          break;
        }
        case "Category": {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
        case "SubCategory": {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
        case "Countries": {
          this.router.navigate(["/Countries/index"]);
          break;
        }
        default: {
          this.router.navigate(["/LoginComponent"]);
          break;
        }
      }
    }
  };
  switchStatus = function (status: number, msg: string | undefined) {
    switch (true) {
      case status >= 400 && 500 > status: {
        Swal.fire("خطأ...", msg, "error");
        break;
      }
      case status <= 500: {
        Swal.fire("خطأ...", msg, "error");
        break;
      }
      default: {
        Swal.fire("خطأ...", msg, "error");
        break;
      }
    }
  };
}
