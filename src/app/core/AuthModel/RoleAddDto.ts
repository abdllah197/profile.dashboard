export class RoleAddDto {
  userId: string | undefined;
  roles: any[] | undefined;
}
