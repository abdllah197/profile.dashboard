export interface UserData {
  id: string;
  isAuthenticated: boolean;
  username: string;
  email: string;
  roles: string[];
  token: string;
  expiresOn: Date;
  refreshTokenExpiration: Date;
}

export interface LoginObject {
  isSuccess: boolean;
  errorMessage?: any;
  userData: UserData;
  responseCode: string;
}
