import { Injectable } from '@angular/core';
import Swal from "sweetalert2";
import { CoreCrudService } from './../DynamicCrud/core-crud.service';
import { BranchDto } from './../../branch/Models/BranchDto';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { CanActivate } from "@angular/router";
@Injectable({
  providedIn: 'root'
})
export class BranchIdService implements CanActivate {
  constructor(private coreCrudService: CoreCrudService,private router: Router,private location: Location,){  
    
  }
  canActivate() {
    let user=localStorage.getItem("user")
    if(user){      
      this.GetBranch();
    } 
    else{
      localStorage.removeItem("branchId")
    }
    return true;
  }
   async GetBranch() {
    return new Promise((resolve) => { 
      let dataBranch:BranchDto[];       
    this.coreCrudService.GetData("", "GetAllBranches").subscribe(
      (res) => {
        dataBranch=res;        
        
        let branchId:string =localStorage.getItem('branchId');
        var inputOptions = {};
        
         for(let item of dataBranch){
          inputOptions[item.id] = item.nameEn;
         }
          if(branchId==null)
          {
            (async () => {
     
              const { value: string } = await Swal.fire({
                title: 'Select branch first',
                input: 'select',
                inputOptions:inputOptions,
                inputPlaceholder: 'Select a branch',
                showCancelButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false,        
                inputValidator: (value) => {
                  return new Promise((resolve) => {            
                    if (value) {
                      localStorage.setItem('branchId',value);                      
                      resolve('')
                    } else {
                      resolve('You need to select first')
                    }
                  })
                }
              })      
             
              })().then(()=>{
                this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
                  this.router.navigate([this.location.path()]);
              }); 
              });
              
          }          
      },
      
      (err) => {}
    );    
      resolve('');
    });    
  } 
}
