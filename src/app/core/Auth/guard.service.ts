import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { EncryptStorage } from "encrypt-storage";

@Injectable({
  providedIn: "root",
})
export class GuardService implements CanActivate {
  constructor(private router: Router) {}
  canActivate() {
    var user = JSON.parse(JSON.stringify(localStorage.getItem("user")!));
    try { 
      if(user && !!user.includes("admin")){
        
          //console.log(user);
          return true;
         
      }      
      this.router.navigate(["/login"]);
      return false;
         
     
    } catch (e) {
      console.log(e);
      return true;
    }
  }
}
