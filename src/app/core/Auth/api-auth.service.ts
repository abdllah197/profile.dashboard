import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams,
} from "@angular/common/http";
import { LoginDto } from "../AuthModel/LoginDto";
import { LoginObject } from "../AuthModel/UserModel";
import { Router,ActivatedRoute  } from "@angular/router";
import { Helper } from "../Helper";
import { UpdateDto } from "../AuthModel/UpdateDto";
import Swal from "sweetalert2";
import { RoleAddDto } from "../AuthModel/RoleAddDto";
import { NotifyDto } from "../AuthModel/NotifyDto";
import { environment } from "../../../environments/environment";
@Injectable({
  providedIn: "root",
})
export class ApiAuthService {
  //https://www.npmjs.com/package/encrypt-storage#asyncencryptstorage
  apiRoot: string = environment.baseUrl;
  lang: string = "ar";
  headers: HttpHeaders | undefined;
  helper: Helper;

  constructor(private http: HttpClient, private router: Router,private route: ActivatedRoute) {
    const headers = { lang: this.lang };
    var token = JSON.parse(JSON.stringify(localStorage.getItem("token")!));
    if (!!token) {
      this.headers = new HttpHeaders(headers).set(
        "Authorization",
        `Bearer ${token}`
      );
    }
    this.helper = new Helper(this.router);
  }
  login(loginDto: LoginDto) {
    const params = new HttpParams()
      .set("email", loginDto.email)
      .set("lang", loginDto.lang)
      .set("password", loginDto.password);
    return this.http.post(this.apiRoot + "Login", params).subscribe(
      (res) => {
        let userModel = res as LoginObject;
        //console.log(JSON.stringify(userModel.userData.roles));
        localStorage.setItem("token", JSON.stringify(userModel.userData.token));
        localStorage.setItem(
          "user",
          JSON.stringify({
            id: userModel.userData.id,
            username: userModel.userData.username,
            email:
              userModel.userData.email == null ? "" : userModel.userData.email,
            roles:
              userModel.userData.roles == null ? [] : userModel.userData.roles,
          })
        );
        this.helper.switchRole(userModel.userData.roles);        
      },
      (err) => {
        //function handel status code
        if (err instanceof HttpErrorResponse) {
          console.log(err.error.responseCode);
          this.helper.switchStatus(err.status, err.error.responseCode);
        }
      }
    );
  }
  UpdateProfile(updateDto: UpdateDto) {
    let data = new FormData();
    data.append("email", updateDto.email);
    data.append("imgProfile", updateDto.imgProfile);
    data.append("lang", updateDto.lang);
    data.append("phone", updateDto.phone);
    data.append("userName", updateDto.userName);

    return this.http
      .put(this.apiRoot + "UpdateProfile", data, { headers: this.headers })
      .subscribe(
        (res) => {
          let userModel = res as LoginObject;
          localStorage.removeItem("user");
          localStorage.setItem(
            "user",
            JSON.stringify({
              id: userModel.userData.id,
              userName: userModel.userData.username,
              email:
                userModel.userData.email == null
                  ? ""
                  : userModel.userData.email,
              roleName:
                userModel.userData.roles == null
                  ? []
                  : userModel.userData.roles,
            })
          );
          Swal.fire({
            text: "تم التعديل بنجاح",
            icon: "success",
          });
          location.reload();
        },
        (err) => {
          //function handel status code
          if (err instanceof HttpErrorResponse) {
            console.log(err.error);
            this.helper.switchStatus(err.status, err.error.msg);
          }
        }
      );
  }

  GetUserAndRoles() {
    return this.http.get(this.apiRoot + "ListUsersWithRoles");
  }
  GetRoles() {
    return this.http.get(this.apiRoot + "ListRoles");
  }
  AddUserToRoles(roleAddDto: RoleAddDto) {
    let data = new FormData();
    data.append("userId", roleAddDto.userId!);
    data.append("rolesName", JSON.stringify(roleAddDto.roles));
    return this.http.post(this.apiRoot + "AddUserToRoles", data);
  }
  SendNotify(notifyDto: NotifyDto) {
    let data = new FormData();
    data.append("msgAr", notifyDto.msgAr);
    data.append("msgEn", notifyDto.msgEn);
    data.append("usersId", JSON.stringify(notifyDto.usersId));
    console.log(JSON.stringify(notifyDto.usersId));
    return this.http.post(this.apiRoot + "SendNotify", data);
  }

  ListUsersDb() {
    return this.http.get(this.apiRoot + "ListUsers");
  }
  ListProvidersDb() {
    return this.http.get(this.apiRoot + "ListProviders");
  }
}
