import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class GuardLoggedInService implements CanActivate {
  constructor(private router: Router) {}
  canActivate() {
    var user = JSON.parse(JSON.stringify(localStorage.getItem("user")!));
    try { 
      if(user && !!user.includes("admin")){
        this.router.navigate(["/dashboard"]);
          //console.log(user);
          return false;         
      }      
      
      return true;
         
     
    } catch (e) {
      console.log(e);
      return true;
    }
  }
}
