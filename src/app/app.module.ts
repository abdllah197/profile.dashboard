import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { AppRoutingModule } from "./app.routing";
import { ComponentsModule } from "./components/components.module";
import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { LoginComponent } from "./login/login.component";
import { CoreModule } from "./core/core.module";
import { ApiAuthService } from "./core/Auth/api-auth.service";
import { DataTablesModule } from "angular-datatables";
import {GuardService} from "./core/Auth/guard.service"

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    DataTablesModule,
  ],
  declarations: [AppComponent, AdminLayoutComponent, LoginComponent],
  providers: [ApiAuthService,GuardService],
  bootstrap: [AppComponent],
})
export class AppModule {}
