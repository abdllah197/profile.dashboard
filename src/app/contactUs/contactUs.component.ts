import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CoreCrudService } from "app/core/DynamicCrud/core-crud.service";
import { GenericContactUsDto, ContactUsDto } from "./Models/contactUsDto";
import Swal from "sweetalert2";
import { BranchDto } from "./../branch/Models/BranchDto";
import { environment } from "../../environments/environment";

@Component({
  selector: "app-contactUs",
  templateUrl: "./contactUscomponent.html",
  styleUrls: ["./contactUs.component.scss"],
})
export class ContactUsComponent {
  localBranchId=localStorage.getItem('branchId');
  imageUrl=environment.baseUrlImages;
  data: ContactUsDto;
  dataBranch: BranchDto;
  dataProperty = Array<GenericContactUsDto>();
  IsAllowEdit: boolean = false;
  IsAllowAdd: boolean = false;
  IsAllowDelete: boolean = true;
  IsAllowReply: boolean = true;
  Id: any;
  AddForm: FormGroup;
  EditForm: FormGroup;
  ReplyForm: FormGroup;
  selectedFile!: File;

  constructor(private coreCrudService: CoreCrudService) {
    this.showPropName(new ContactUsDto());
    this.Get();
    this.GetBranch();
  }
  public uploadFile = (files: any) => {
    if (files.length === 0) {
      return;
    }
    this.selectedFile = <File>files[0];
  };

  showPropName(Model: any) {
    const AddformGroupFields = {};
    const EditformGroupFields = {};
    const ReplyformGroupFields = {};
    for (const [key, value] of Object.entries(Model)) {
      var dto = new GenericContactUsDto();
      dto.Key = key;
      dto.ClassHtml = dto.switchClass(key);
      dto.Type = dto.switchValue(typeof value, key);
      dto.IsRequired = dto.CheckRequired(key);
      dto.IsHide = dto.HideProprty(key);
      dto.KeyAr = dto.CheckLocaliztionAr(key);
      dto.KeyEn = dto.CheckLocaliztionEn(key);
      dto.KeyAdd = "Add-" + key;
      dto.KeyEdit = "Edit-" + key;
      this.dataProperty.push(dto);
      //console.log(dto);
      AddformGroupFields[dto.KeyAdd] = new FormControl("", [
        Validators.required,
      ]);
      EditformGroupFields[dto.KeyEdit] = new FormControl("", [
        Validators.required,
      ]);     
    }
    this.AddForm = new FormGroup(AddformGroupFields);
    this.EditForm = new FormGroup(EditformGroupFields);
    this.ReplyForm= new FormGroup({
      email: new FormControl('',[Validators.email,Validators.required]),
      subject: new FormControl('',[Validators.required]),
      message: new FormControl('',[Validators.required])
    });
  }
  ShowId(data: any) {
    this.dataProperty.forEach((key) => {
      if (key.Type == "checkbox") {
        $("#" + key.KeyEdit).prop("checked", data[key.Key]);
      } else if (key.Type == "date") {
        var now = new Date(data[key.Key]);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + month + "-" + day;
        $("#" + key.KeyEdit).val(today);
      } else if (key.Type == "file") {
        // this.EditForm.get(key.KeyEdit)?.setValue(data[key.Key]);
      } else {
        this.EditForm.get(key.KeyEdit)?.setValue(data[key.Key]);
      }
    });
  }
  ShowReplyId(data: any) {
    this.ReplyForm.get("email")?.setValue(data.email);
  }
  Get() {
    this.coreCrudService.GetData("", this.dataProperty[0].UrlGetAll).subscribe(
      (res) => {
        this.data = res as ContactUsDto;
      },
      (err) => {}
    );
  }
  GetBranch() {
    this.coreCrudService.GetData("", this.dataProperty[0].UrlSelect).subscribe(
      (res) => {
        this.dataBranch = res as BranchDto;
      },
      (err) => {}
    );
  }
  Edit() {
    let valid:Boolean=true;
    let dataEdit = new FormData();
    this.dataProperty.forEach((key) => {
      if (key.Key == "image") {
        dataEdit.append(key.Key, this.selectedFile);
        if(this.selectedFile==undefined&& key.IsRequired){valid=false} 
      } else if (key.Type == "checkbox") {
        if ($("#" + key.KeyEdit).is(":checked")) {
          dataEdit.append(key.Key, "true");
        } else {
          dataEdit.append(key.Key, "false");
        }
      } else if (key.Type == "date") {
        var dateValue = $("#" + key.KeyEdit).val();
        dataEdit.append(key.Key, dateValue.toString());
        if(Date.parse(dateValue.toString()) && key.IsRequired){valid=false} 
      } else if (key.Type == "option") {
        var dateValue = $("#" + key.KeyEdit).val();
        dataEdit.append(key.Key, dateValue.toString());
        if(dateValue.toString()==''&& key.IsRequired){valid=false}
      } else {
        dataEdit.append(key.Key, this.EditForm.get(key.KeyEdit)?.value);
        if(this.AddForm.get(key.KeyAdd)?.value==""&& key.IsRequired){valid=false}
      }
    });
    this.coreCrudService.Update(dataEdit, this.dataProperty[0].UrlEdit);
    setTimeout(() => {
      this.EditForm.reset();
      $("#EditModal .close").click();
      location.reload();
    }, 800);
  }
  get email() { return this.ReplyForm.get('email'); }
  get subject() { return this.ReplyForm.get('subject'); }
  get message() { return this.ReplyForm.get('message'); }
  Reply(){
    
    if(this.ReplyForm.valid){
      let dataReply = new FormData();
      dataReply.append("Email", this.ReplyForm.get("email")?.value);
      dataReply.append("Subject", this.ReplyForm.get("subject")?.value);
      dataReply.append("Message", this.ReplyForm.get("message")?.value);      
      this.coreCrudService.SendEmail(dataReply, this.dataProperty[0].UrlSendEmail);
      setTimeout(() => {
        this.ReplyForm.reset();
        $("#ReplyModal .close").click();
        //location.reload();
      }, 800);
    }
   
  }
  Add() {
    let valid:Boolean=true;
      let dataAdd = new FormData();
      this.dataProperty.forEach((key) => {
        if (key.Key == "image") {
          dataAdd.append(key.Key, this.selectedFile);
          if(this.selectedFile==undefined&& key.IsRequired){valid=false}          
        } else if (key.Type == "checkbox") {
          if ($("#" + key.KeyAdd).is(":checked")) {
            dataAdd.append(key.Key, "true");
          } else {
            dataAdd.append(key.Key, "false");
          }
        } else if (key.Type == "date") {
          var dateValue = $("#" + key.KeyAdd).val();
          dataAdd.append(key.Key, dateValue.toString());
          if(Date.parse(dateValue.toString()) && key.IsRequired){valid=false} 
        } else if (key.Type == "option") {
          var option = $("#" + key.KeyAdd).val();
          dataAdd.append(key.Key, option.toString());
          if(option.toString()==''&& key.IsRequired){valid=false} 
        } else if (key.Key == "branchId") {        
          dataAdd.append(key.Key, localStorage.getItem('branchId'));
        } else {
          dataAdd.append(key.Key, this.AddForm.get(key.KeyAdd)?.value);
          if(this.AddForm.get(key.KeyAdd)?.value==""&& key.IsRequired){valid=false} 
        }
      });
      if(valid){
      this.coreCrudService.AddNew(dataAdd, this.dataProperty[0].UrlAdd);

      setTimeout(() => {
        this.AddForm.reset();
        $("#AddModal .close").click();
        location.reload();
      }, 800);
    }
  }
  Delete(id) {
    Swal.fire({
      title: "هل انت متاكد ؟",
      text: "لن تستطيع استرجاع البيانات المحذوفه!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "نعم احذف!",
      cancelButtonText: "لا تحذف انتظر",
    }).then((result) => {
      if (result.value) {
        Swal.fire("Deleted!", "تم الحذف بنجاح", "success");
        this.coreCrudService
          .Delete(id, this.dataProperty[0].UrlDelete)
          .subscribe(
            (res) => {
              location.reload();
            },
            (err) => {}
          );
        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("الغاء الحذف", "تم الالغاء بنجاح", "error");
      }
    });
  }
}
