import { BranchDto } from './../../branch/Models/BranchDto';

export class ContactUsDto {
  id: number = 0;
  email: string = "";
  phone: string = "";
  name: string = "";
  subject: string = "";
  message: string = "";
  date: Date = new Date();
  branchId: Array<BranchDto> = new Array<BranchDto>();
}
export class GenericContactUsDto {
  Key = "";
  KeyAr = "";
  KeyEn = "";
  KeyEdit = "";
  KeyAdd = "";
  Type = "";
  ClassHtml = "";
  IsRequired = false;
  IsHide = false;
  UrlGetAll = "GetAllContactUs";
  UrlAdd = "";
  UrlSendEmail = "SendEmail";
  UrlEdit = "";
  UrlDelete = "DeleteContactUs";
  UrlSelect = "GetAllBranches";
  switchClass = (key) => {
    if (key == "image") {
      return "custom-file-input";
    } else {
      return "form-control";
    }
  };
  switchValue = (value, key) => {
    if (key == "branchDtoId") {
      return "option";
    }
    if (key == "date") {
      return "date";
    }
    if (key == "image" || key == "video") {
      return "file";
    }
    if (value == "boolean") {
      return "checkbox";
    }
    if (value == "string") {
      return "text";
    }
    if (value == "number") {
      return "number";
    }
  };
  CheckRequired = (value) => {
    switch (value) {
      case "id": {
        return false;
      }
      case "isActive": {
        return false;
      }
      case "isEducation": {
        return false;
      }
      case "isExperience": {
        return false;
      }
      case "date": {
        return false;
      }
      case "nameAr": {
        return true;
      }
      case "nameEn": {
        return true;
      }
    }
  };
  CheckLocaliztionAr = (value) => {
    switch (value) {
      case "id": {
        return "رقم";
      }
      case "isActive": {
        return "تفعيل";
      }
      case "date": {
        return "التاريخ";
      }
      case "nameAr": {
        return "الاسم عربى";
      }
      case "nameEn": {
        return "الاسم انجليزى";
      }
      case "image": {
        return "صوره";
      }
    }
  };
  CheckLocaliztionEn = (value) => {
    switch (value) {
      case "id": {
        return "Id";
      }
      case "isActive": {
        return "Active";
      }
      case "isEducation": {
        return "Education";
      }
      case "isExperience": {
        return "Experience";
      }
      case "date": {
        return "Date";
      }
      case "name": {
        return "Name";
      }
      case "subject": {
        return "Subject";
      }
      case "email": {
        return "Email";
      }
      case "phone": {
        return "Phone";
      }
      case "message": {
        return "Message";
      }
      case "image": {
        return "image";
      }
      case "branchId": {
        return "branchId";
      }
      case "branchNameAr": {
        return "branch Name Ar";
      }
      case "branchNameEn": {
        return "branch name En";
      }
    }
  };
  HideProprty = (value) => {
    switch (value) {
      case "id": {
        return true;
      }
      case "branchId": {
        return true;
      }     
    }
  };
}
