import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ApiAuthService } from "app/core/Auth/api-auth.service";
import { LoginDto } from "app/core/AuthModel/LoginDto";
import Swal from "sweetalert2";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  valid: FormGroup;
  data: Array<string> | undefined;
  constructor(
    private apiAuthService: ApiAuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.valid = fb.group({
      email: ["", Validators.compose([Validators.required, Validators.email])],
      password: ["", Validators.required],
    });
  }
  Login() {
    if (this.valid.invalid) {
      Swal.fire({
        title: "خطأ",
        text: "من فضلك ادخل الحقول الفارغه",
        icon: "error",
        confirmButtonText: "موافق",
      });
      return false;
    }
    let LoginModel = new LoginDto();
    LoginModel.email = this.valid.get("email")?.value;
    LoginModel.lang = "ar";
    LoginModel.password = this.valid.get("password")?.value;

    this.apiAuthService.login(LoginModel);
    return true;
  }

  opensweetalert() {
    Swal.fire({
      text: "Hello!",
      icon: "success",
    });
  }
  opensweetalertdng() {
    Swal.fire("Oops...", "Something went wrong!", "error");
  }

  opensweetalertcst() {
    Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this imaginary file!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, keep it",
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          "Deleted!",
          "Your imaginary file has been deleted.",
          "success"
        );
        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
  }
}
